﻿// Met using kan je een XNA codebibliotheek toevoegen en gebruiken in je class
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class ExplorerManager
    {
        // Fields
        private static Level level;

        private static Explorer explorer;

        

        // Properties
        public static Level Level
        {
            set { level = value; }
        }

        public static Explorer Explorer
        {
            set { explorer = value; }
        }

        // Constructor
        public ExplorerManager()
        {

        }

        // Methods
        public static bool CollisionDetectionExplorerWalls()
        {
            for (int i = 0; i < level.Blocks.GetLength(0); i++)
            {
                for (int j = 0; j < level.Blocks.GetLength(1); j++)
                {
                    if (level.Blocks[i, j].Passable == false)
                    {
                        if (explorer.CollisionRect.Intersects(level.Blocks[i, j].Rectangle))
                        {
                            Console.WriteLine("Ik raak muur element i " + i + " j " + j);
                            return true;
                        }
                    }
                    
                }
            }
                return false;
        }

        public static void CollisionDetectionTreasures()
        {
            foreach (Image image in level.Treasures)
            {
                if (explorer.CollisionRect.Intersects(image.Rectangle))
                {
                    level.Treasures.Remove(image);
                    break;
                }
            }
        }
    }
}