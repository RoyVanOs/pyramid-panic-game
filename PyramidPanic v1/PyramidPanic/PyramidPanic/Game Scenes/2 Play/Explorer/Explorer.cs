﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class Explorer : IAnimatedSprite
    {
        //Fields
        // Maakt een instantie game aan van het type PyramidPanic
        private PyramidPanic game;

        // Maakt een instantie state aan van het type IEntityState
        private IEntityState state;

        // Maakt een instantie texture aan van het type Texture2D 
        private Texture2D texture;

        // Maakt een instantie position aan van het type Vector2
        private Vector2 position;

        // Maakt een instantie speed aan met de waarde 4 van het type int
        private int speed = 4;

        //Toestanden
        // Maakt de idleWalk aan
        private ExplorerIdleWalk idleWalk;

        // Maakt de idle aan
        private ExplorerIdle idle;

        // Maakt de walkUp aan
        private ExplorerWalkUp walkUp;

        // Maakt de walkDown aan
        private ExplorerWalkDown walkDown;

        // Maakt de walkLeft aan
        private ExplorerWalkLeft walkLeft;

        // Maakt de walkRight aan
        private ExplorerWalkRight walkRight;

        private Rectangle collisionRect;


        //Properties
        #region 

        // Getter voor IdleWalk
        public ExplorerIdleWalk IdleWalk
        {
            get { return this.idleWalk; }
        }

        // Getter voor Idle
        public ExplorerIdle Idle
        {
            get { return this.idle; }
        }

        // Getter voor WalkUp
        public ExplorerWalkUp WalkUp
        {
            get { return this.walkUp; }
        }

        // Getter voor WalkDown
        public ExplorerWalkDown WalkDown
        {
            get { return this.walkDown; }
        }

        // Getter voor WalkLeft
        public ExplorerWalkLeft WalkLeft
        {
            get { return this.walkLeft; }
        }

        // Getter voor WalkRight
        public ExplorerWalkRight WalkRight
        {
            get { return this.walkRight; }
        }

        // Setter voor State
        public IEntityState State
        {
            set { 
                    this.state = value;
                    this.state.Initialize();
                }
        }

        // Getter voor Game
        public PyramidPanic Game
        {
            get { return this.game; }
        }

        // Getter voor Speed
        public int Speed
        {
            get { return this.speed; }
        }

        // Getter voor Texture
        public Texture2D Texture
        {
            get { return this.texture; }
        }

        // Getter en Setter voor Position
        public Vector2 Position
        {
            get { return this.position; }
            set { 
                this.position = value;
                this.collisionRect.X = (int)this.position.X - 16;
                this.collisionRect.Y = (int)this.position.Y - 16;
            }
        }

        public Rectangle CollisionRect
        {
            get { return this.collisionRect; }
        }

        #endregion

        //Constructor
        public Explorer(PyramidPanic game, Vector2 position)
        {
            this.game = game;
            this.position = position;

            // Laadt de sprite texture voor de Explorer
            this.texture = game.Content.Load<Texture2D>(@"AI\Explorer\Explorer");

            // Zorgt ervoor dat de Explorer de IdleWalk state kan gebruiken
            this.idleWalk = new ExplorerIdleWalk(this);

            // Zorgt ervoor dat de Explorer de Idle state kan gebruiken
            this.idle = new ExplorerIdle(this);

            // Zorgt ervoor dat de Explorer de WalkUp state kan gebruiken
            this.walkUp = new ExplorerWalkUp(this);

            // Zorgt ervoor dat de Explorer de WalkDown state kan gebruiken
            this.walkDown = new ExplorerWalkDown(this);

            // Zorgt ervoor dat de Explorer de WalkDown kan state gebruiken
            this.walkLeft = new ExplorerWalkLeft(this);

            // Zorgt ervoor dat de Explorer de WalkDown kan state gebruiken
            this.walkRight = new ExplorerWalkRight(this);

            // Startstate van de Explorer
            this.state = this.idle;

            this.collisionRect = new Rectangle((int)position.X - 16, (int)position.Y - 16, 32, 32);
        }

        //Update methode
        public void Update(GameTime gameTime)
        {
            this.state.Update(gameTime);
            ExplorerManager.CollisionDetectionTreasures();
        }

        //Draw methode
        public void Draw(GameTime gameTime)
        {
            this.state.Draw(gameTime);
        }
    }
}
