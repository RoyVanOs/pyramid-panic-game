﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class ExplorerWalkUp : AnimatedSprite, IEntityState
    {
        //Fields
        // Maakt een instantie explorer aan van het type Explorer
        private Explorer explorer;

        // Maakt een instantie velocity aan van het type Vector2
        private Vector2 velocity;

        //Constructor
        public ExplorerWalkUp(Explorer explorer) : base(explorer)
        {
            this.explorer = explorer;

            // Geeft de destinationRectangle de waardes van de positie en grootte van de explorer
            this.destinationRectangle = new Rectangle((int)this.explorer.Position.X,
                                                      (int)this.explorer.Position.Y,
                                                       this.explorer.Texture.Width / 4,
                                                       this.explorer.Texture.Height);

            // Geeft de velocity de snelheid in Y waarde
            this.velocity = new Vector2(0f, this.explorer.Speed);

            // Geeft de rotation de waarde 90°
            this.rotation = (float)Math.PI / 2;

            // Geeft het effect de waarde FlipHorizontally
            this.effect = SpriteEffects.FlipHorizontally;
        }

        // Initialize method
        public void Initialize()
        {
            // Geeft de destination de X waarde van de explorer
            this.destinationRectangle.X = (int)this.explorer.Position.X;

            // Geeft de destination de Y waarde van de explorer
            this.destinationRectangle.Y = (int)this.explorer.Position.Y;
        }
        
        // Update method
        public new void Update(GameTime gameTime)
        {
            // Roept de helper method ExplorerMoveUp aan
            this.ExplorerMoveUp();

            // Roept de helper method EdgeDetectKeyUp_W aan
            this.EdgeDetectKeyUp_W();

            base.Update(gameTime);
        }

        // Draw method
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
        // HELPERS
        // Helper method voor beweging naar boven
        private void ExplorerMoveUp()
        {
            // Geeft de velocity de snelheid in Y waarde
            this.explorer.Position -= this.velocity;
            if (ExplorerManager.CollisionDetectionExplorerWalls())
            {
                this.explorer.Position += this.velocity;
            }
            // Roept de Initialize method van deze class aan
            this.Initialize();
        }

        // Helper method voor het detecteren of de W toets losgelaten wordt
        private void EdgeDetectKeyUp_W()
        {
            // Voert code uit wanneer de W toets losgelaten wordt
            if (Input.LevelDetectKeyUp(Keys.W))
            {
                // Bereken de modulo waarde van 32
                int modulo = (int)this.explorer.Position.Y % 32;
                Console.WriteLine(modulo);

                if (modulo >= 16 - this.explorer.Speed && modulo <= 16)
                {
                    //zet de laatste stap op zijn grid
                    int exactly32 = (int)this.explorer.Position.Y / 32;
                    Console.WriteLine(exactly32);
                    this.explorer.Position = new Vector2(this.explorer.Position.X, (exactly32 + 1) * 32 - 16);

                    this.explorer.State = this.explorer.Idle;
                    this.explorer.Idle.Initialize();
                    this.explorer.IdleWalk.Rotation = -(float)Math.PI / 2;
                }
                
            }
        }
    }
}
