﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class ExplorerIdleWalk : AnimatedSprite, IEntityState
    {
        //Fields
        // Maakt een instantie explorer aan van het type Explorer
        private Explorer explorer;

        // Setter voor Effect
        public SpriteEffects Effect
        {
            set { this.effect = value; }
        }

        // Setter voor Rotation
        public float Rotation
        {
            set { this.rotation = value; }
        }

        //Constructor
        public ExplorerIdleWalk(Explorer explorer) : base(explorer)
        {
            this.explorer = explorer;

            // Geeft de destinationRectangle de waardes van de positie en grootte van de explorer
            this.destinationRectangle = new Rectangle((int)this.explorer.Position.X,
                                                      (int)this.explorer.Position.Y,
                                                       this.explorer.Texture.Width / 4,
                                                       this.explorer.Texture.Height);

            // Geeft het effect de waarde FlipVertically
            this.effect = SpriteEffects.FlipVertically;
        }

        // Initialize methode
        public void Initialize()
        {
            // Geeft de destination de X waarde van de explorer
            this.destinationRectangle.X = (int)this.explorer.Position.X;

            // Geeft de destination de Y waarde van de explorer
            this.destinationRectangle.Y = (int)this.explorer.Position.Y;
        }

        // Update methode
        public new void Update(GameTime gameTime)
        {
            // Voert code uit als de W knop losgelaten wordt
            if (Input.EdgeDetectKeyUp(Keys.W))
            {
                // Verandert de explorer state naar Idle
                this.explorer.State = this.explorer.Idle;

                // Roept de Initialize methode aan van de Idle class
                this.explorer.Idle.Initialize();
            }

            // Voert code uit als de A knop losgelaten wordt
            else if (Input.EdgeDetectKeyUp(Keys.A))
            {
                // Verandert de explorer state naar Idle
                this.explorer.State = this.explorer.Idle;

                // Roept de Initialize methode aan van de Idle class
                this.explorer.Idle.Initialize();
            }

            // Voert code uit als de S knop losgelaten wordt
            else if (Input.EdgeDetectKeyUp(Keys.S))
            {
                // Verandert de explorer state naar Idle
                this.explorer.State = this.explorer.Idle;

                // Roept de Initialize methode aan van de Idle class
                this.explorer.Idle.Initialize();
            }

            // Voert code uit als de D knop losgelaten wordt
            else if (Input.EdgeDetectKeyUp(Keys.D))
            {
                // Verandert de explorer state naar Idle
                this.explorer.State = this.explorer.Idle;

                // Roept de Initialize methode aan van de Idle class
                this.explorer.Idle.Initialize();
            }
            base.Update(gameTime);
        }

        // Draw methode
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
