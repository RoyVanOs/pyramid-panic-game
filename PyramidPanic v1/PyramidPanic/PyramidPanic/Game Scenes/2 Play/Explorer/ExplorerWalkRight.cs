﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class ExplorerWalkRight : AnimatedSprite, IEntityState
    {
        //Fields
        // Maakt een instantie explorer aan van het type Explorer
        private Explorer explorer;

        // Maakt een instantie velocity aan van het type Vector2
        private Vector2 velocity;

        //Constructor
        public ExplorerWalkRight(Explorer explorer) : base(explorer)
        {
            this.explorer = explorer;

            // Geeft de destinationRectangle de waardes van de positie en grootte van de explorer
            this.destinationRectangle = new Rectangle((int)this.explorer.Position.X,
                                                      (int)this.explorer.Position.Y,
                                                       this.explorer.Texture.Width / 4,
                                                       this.explorer.Texture.Height);

            // Geeft de velocity de snelheid in X waarde
            this.velocity = new Vector2(this.explorer.Speed, 0f);

            // Geeft het effect de waarde FlipVertically
            this.effect = SpriteEffects.FlipVertically;

            // Geeft de rotation de waarde 90°
            this.rotation = 0f;
        }

        // Initialize method
        public void Initialize()
        {
            // Geeft de destination de X waarde van de explorer
            this.destinationRectangle.X = (int)this.explorer.Position.X;

            // Geeft de destination de Y waarde van de explorer
            this.destinationRectangle.Y = (int)this.explorer.Position.Y;
        }

        // Update method
        public new void Update(GameTime gameTime)
        {
            // Roept de helper method ExplorerMoveRight aan
            this.ExplorerMoveRight();

            // Roept de helper method EdgeDetectKeyUp_D aan
            this.EdgeDetectKeyUp_D();

            base.Update(gameTime);
        }

        // Draw method
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        // HELPERS
        // Helper method voor beweging naar rechts
        public void ExplorerMoveRight()
        {
            // Geeft de velocity de snelheid in X waarde
            this.explorer.Position += this.velocity;
            if (ExplorerManager.CollisionDetectionExplorerWalls())
            {
                this.explorer.Position -= this.velocity;
            }
            // Roept de Initialize method van deze class aan
            this.Initialize();
        }

        // Helper method voor het detecteren of de D toets losgelaten wordt
        public void EdgeDetectKeyUp_D()
        {
            // Voert code uit wanneer de D toets losgelaten wordt
            if (Input.LevelDetectKeyUp(Keys.D))
            {
                // Bereken de modulo waarde van 32
                int modulo = (int)this.explorer.Position.X % 32;
                Console.WriteLine(modulo);

                if (modulo >= 16 - this.explorer.Speed && modulo <= 16)
                {
                    //zet de laatste stap op zijn grid
                    int exactly32 = (int)this.explorer.Position.X / 32;
                    Console.WriteLine(exactly32);
                    this.explorer.Position = new Vector2((exactly32 + 1) * 32 - 16, this.explorer.Position.Y);

                    this.explorer.State = this.explorer.Idle;
                    this.explorer.Idle.Initialize();
                    this.explorer.IdleWalk.Rotation = (float)Math.PI / 0;
                }
            }
        }
    }
}
