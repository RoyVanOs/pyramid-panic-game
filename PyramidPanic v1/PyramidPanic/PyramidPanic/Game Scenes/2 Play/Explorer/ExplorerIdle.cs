﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class ExplorerIdle : AnimatedSprite, IEntityState
    {
        //Fields
        // Maakt een instantie explorer aan van het type Explorer
        private Explorer explorer;

        // Setter voor Effect
        public SpriteEffects Effect
        {
            set { this.effect = value; }
        }

        // Setter voor Rotation
        public float Rotation
        {
            set { this.rotation = value; }
        }

        //Constructor
        public ExplorerIdle(Explorer explorer) : base(explorer)
        {
            this.explorer = explorer;

            // Geeft de destinationRectangle de waardes van de positie en grootte van de explorer
            this.destinationRectangle = new Rectangle((int)this.explorer.Position.X,
                                                      (int)this.explorer.Position.Y,
                                                       this.explorer.Texture.Width / 4,
                                                       this.explorer.Texture.Height);

            this.sourceRectangle.X = 32;
        }

        // Initialize methode
        public void Initialize()
        {
            // Geeft de destination de X waarde van de explorer
            this.destinationRectangle.X = (int)this.explorer.Position.X;

            // Geeft de destination de Y waarde van de explorer
            this.destinationRectangle.Y = (int)this.explorer.Position.Y;
        }

        public new void Update(GameTime gameTime)
        {
            // Voert code uit als de W toets wordt ingedrukt
            if (Input.EdgeDetectKeyDown(Keys.W))
            {
                // Verandert de state naar WalkUp
                this.explorer.State = this.explorer.WalkUp;

                // Roept de Initialize methode aan van de WalkUp class
                this.explorer.WalkUp.Initialize();

                // Geeft de rotation in Idle de waarde -90°
                this.explorer.Idle.rotation = - (float)Math.PI / 2;

                // Geeft het effect de waarde FlipVertically
                this.explorer.Idle.Effect = SpriteEffects.FlipVertically;
            }

            // Voert code uit als de A toets wordt ingedrukt
            else if (Input.EdgeDetectKeyDown(Keys.A))
            {
                // Verandert de state naar WalkLeft
                this.explorer.State = this.explorer.WalkLeft;

                // Roept de Initialize methode aan van de WalkLeft class
                this.explorer.WalkLeft.Initialize();

                // Geeft de rotation in Idle de waarde 180°
                this.explorer.Idle.rotation = (float)Math.PI / 1;

                // Geeft het effect de waarde FlipVertically
                this.explorer.Idle.Effect = SpriteEffects.FlipVertically;

                
            }

            // Voert code uit als de S toets wordt ingedrukt
            else if (Input.EdgeDetectKeyDown(Keys.S))
            {
                // Verandert de state naar WalkDown
                this.explorer.State = this.explorer.WalkDown;

                // Roept de Initialize methode aan van de WalkDown class
                this.explorer.WalkDown.Initialize();

                // Geeft de rotation in Idle de waarde 90°
                this.explorer.Idle.rotation = (float)Math.PI / 2;

                // Geeft het effect de waarde FlipVertically
                this.explorer.Idle.Effect = SpriteEffects.FlipVertically;
            }

            // Voert code uit als de D toets wordt ingedrukt
            else if (Input.EdgeDetectKeyDown(Keys.D))
            {
                // Verandert de state naar WalkRight
                this.explorer.State = this.explorer.WalkRight;

                // Roept de Initialize methode aan van de WalkRight class
                this.explorer.WalkRight.Initialize();

                // Geeft de rotation in Idle de waarde 0°
                this.explorer.Idle.rotation = 0f;

                // Geeft het effect de waarde FlipVertically
                this.explorer.Idle.Effect = SpriteEffects.FlipVertically;
            }
        }

        // Draw method
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
