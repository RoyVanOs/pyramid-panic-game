﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class Panel
    {
        // Fields
        private List<Image> images;
        private PyramidPanic game;
        private SpriteFont arial;
        private Vector2 position1, position2;

        // Properties

        // Constructor
        public Panel(PyramidPanic game, Vector2 position)
        {
            this.game = game;
            this.images = new List<Image>();
            this.images.Add(new Image(game, @"PlayScene\panel", new Vector2(0, 992)));
            this.images.Add(new Image(game, @"PlayScene\Scarab", new Vector2(680, 1030)));
            this.images.Add(new Image(game, @"PlayScene\Lives", new Vector2(240, 1030)));

            this.arial = game.Content.Load<SpriteFont>(@"Fonts\Arial");
            this.position1 = new Vector2(280, 1030);
            this.position2 = new Vector2(720, 1030);
        }

        // Update

        // Draw
        public void Draw(GameTime gameTime)
        {
            foreach (Image image in this.images)
            {
                image.Draw(gameTime);
            }
            this.game.SpriteBatch.DrawString(this.arial, "1", this.position1, Color.Yellow);
            this.game.SpriteBatch.DrawString(this.arial, "1", this.position2, Color.Yellow);
        }

        // Helper methods
    }
}
