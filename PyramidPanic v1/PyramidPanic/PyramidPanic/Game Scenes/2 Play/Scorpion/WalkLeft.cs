﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class WalkLeft : AnimatedSprite, IEntityState
    {
        //Fields
        private Scorpion scorpion;
        private Vector2 velocity;

        public Rectangle DestinationRect
        {
            get { return this.destinationRectangle; }
        }

        //Constructor
        public WalkLeft(Scorpion scorpion) : base(scorpion)
        {
            // Reference van de Scorpion class
            this.scorpion = scorpion;

            // Geeft de sprite die aan de WalkLeft vast zit een effect
            this.effect = SpriteEffects.FlipHorizontally;

            // Geeft de destinationRectangle de waardes van de positie en grootte van de explorer
            this.destinationRectangle = new Rectangle((int)this.scorpion.Position.X,
                                                      (int)this.scorpion.Position.Y,
                                                       this.scorpion.Texture.Width / 4,
                                                       this.scorpion.Texture.Height);

            // Geeft de velocity de snelheid in X waarde
            this.velocity = new Vector2(this.scorpion.Speed, 0f);
        }

        public void Initialize()
        {
            // Geeft de destination de X waarde van de beetle
            this.destinationRectangle.X = (int)this.scorpion.Position.X;

            // Geeft de destination de Y waarde van de beetle
            this.destinationRectangle.Y = (int)this.scorpion.Position.Y;
        }

        // Update method
        public new void Update(GameTime gameTime)
        {
            // Zorgt ervoor dat de scorpion naar links beweegt
            this.ScorpionMoveLeft();
            base.Update(gameTime);
        }

        // Draw method
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        private void ScorpionMoveLeft()
        {
            // Voert code uit als de scorpion een kleinere y-waarde dan 0 - spritewidth / 2 bereikt
            if (this.scorpion.Position.X < this.scorpion.LeftBorder)
            {
                // Slaat de state WalkLeft om naar WalkRight
                this.scorpion.State = this.scorpion.WalkRight;
                // Roept de Initialize methode aan van de WalkRight class
                this.scorpion.WalkRight.Initialize();
            }
            this.scorpion.Position -= this.velocity;
            
            // Roept de Initialize methode aan
            this.Initialize();
        }
    }
}
