﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class WalkRight : AnimatedSprite, IEntityState
    {
        //Fields
        private Scorpion scorpion;
        private Vector2 velocity;

        public Rectangle DestinationRect
         {
             get { return this.destinationRectangle; }
         }

        //Constructor
        public WalkRight(Scorpion scorpion) : base(scorpion)
        {
            // Reference van de Scorpion class
            this.scorpion = scorpion;

            // Geeft de positie en de grootte van de destinationRectangle aan
            this.destinationRectangle = new Rectangle((int)this.scorpion.Position.X,
                                                      (int)this.scorpion.Position.Y,
                                                       32,
                                                       32);

            // Geeft de velocity Vector2 waardes
            
            this.velocity = new Vector2(this.scorpion.Speed, 0f);
        }

        // Initialize methode
        public void Initialize()
        {
            // Geeft de destination de X waarde van de beetle
            this.destinationRectangle.X = (int)this.scorpion.Position.X;

            // Geeft de destination de Y waarde van de beetle
            this.destinationRectangle.Y = (int)this.scorpion.Position.Y;
        }

        // Update methode
        public new void Update(GameTime gameTime)
        {
            // Helper method die ervoor zorgt dat de scorpion naar rechts beweegt
            this.ScorpionMoveRight();
            base.Update(gameTime);
        }

        // Draw methode
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        // HELPERS
        // Helper method voor beweging scorpion
        public void ScorpionMoveRight()
        {
            // Voert code uit als de scorpion een grotere x-waarde dan 1920 - spritewidth / 2 bereikt
            if (this.scorpion.Position.X > this.scorpion.RightBorder)
            {
                // Slaat de state WalkRight om naar WalkLeft
                this.scorpion.State = this.scorpion.WalkLeft;
                // Roept de Initialize methode aan van de WalkLeft class
                this.scorpion.WalkLeft.Initialize();
            }
            this.scorpion.Position += this.velocity;

            // Roept de Initialize methode aan
            this.Initialize();
        }
    }
}
