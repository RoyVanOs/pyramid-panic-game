﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class Scorpion : IAnimatedSprite
    {
        //Fields
        // Maakt de game aan van het type PyramidPanic
        private PyramidPanic game;

        // Maakt de state aan van het type IEntityState
        private IEntityState state;

        // Maakt de texture aan van het type Texture2D
        private Texture2D texture;

        // Maakt de position aan van het type Vector2
        private Vector2 position;

        // Maakt de speed aan met een waarde van 3 van het type int
        private int speed = 3;

        //Toestanden
        // Maakt de WalkLeft aan
        private WalkLeft walkLeft;

        // Maakt de WalkRight aan
        private WalkRight walkRight;

        private Rectangle collisionRect;

        private int leftBorder, rightBorder;

        //Properties
        #region Properties

        // Getter voor WalkLeft
        public WalkLeft WalkLeft
        {
            get { return this.walkLeft; }
        }

        // Getter voor WalkRight
        public WalkRight WalkRight
        {
            get { return this.walkRight; }
        }

        // Setter voor State
        public IEntityState State
        {
            set { this.state = value; }
        }

        // Getter voor Game
        public PyramidPanic Game
        {
            get { return this.game; }
        }

        // Getter voor Speed
        public int Speed
        {
            get { return this.speed; }
        }

        // Getter voor Texture
        public Texture2D Texture
        {
            get { return this.texture; }
        }

        // Getter en Setter voor Position
        public Vector2 Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public Rectangle CollisionRect
        {
            
            get {
                this.collisionRect.X = (int)this.position.X;
                return this.collisionRect; 
                }
        }

        public int RightBorder
        {
            get { return this.rightBorder; }
            set { this.rightBorder = value; }
        }

        public int LeftBorder
        {
            get { return this.leftBorder; }
            set { this.leftBorder = value; }
        }
        #endregion

        //Constructor
        public Scorpion(PyramidPanic game, Vector2 position)
        {
            this.game = game;
            this.position = position;

            // Laadt de sprite texture voor de Beetle
            this.texture = game.Content.Load<Texture2D>(@"AI\Scorpion\Scorpion");

            // Zorgt ervoor dat de Scorpion de WalkLeft state kan gebruiken
            this.walkLeft = new WalkLeft(this);

            // Zorgt ervoor dat de Scorpion de WalkRight state kan gebruiken
            this.walkRight = new WalkRight(this);

            // Zorgt ervoor dat de Scorpion in de WalkRight state begint
            this.state = new WalkRight(this);

            this.collisionRect = new Rectangle((int)this.position.X - 16, (int)this.position.Y - 16, 32 * 4, 32);
        }

        //Update method
        public void Update(GameTime gameTime)
        {
            this.state.Update(gameTime);
        }

        //Draw method
        public void Draw(GameTime gameTime)
        {
            this.state.Draw(gameTime);
        }
    }
}
