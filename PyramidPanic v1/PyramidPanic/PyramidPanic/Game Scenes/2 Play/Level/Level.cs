﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;


namespace PyramidPanic
{
    public class Level
    {
        // Fields
        private PyramidPanic game;
        private Explorer explorer;
        private int levelIndex;
        private Stream stream;
        private List<String> lines;
        private Block[,] blocks;
        private List<Scorpion> scorpions;
        private List<Beetle> beetles;
        private List<Image> treasures;
        private Panel panel;
        
        // Properties

        public List<Image> Treasures
        {
            get { return this.treasures; }
        }

        public PyramidPanic Game
        {
            get { return this.game; }
        }

        public int LevelIndex
        {
            get { return this.levelIndex; }
        }

        public List<Scorpion> Scorpions
        {
            get { return this.scorpions; }
        }

        public List<Beetle> Beetles
        {
            get { return this.beetles; }
        }

        public Block[,] Blocks
        {
            get { return this.blocks; }
        }


        // Constructor
        public Level(PyramidPanic game, int levelIndex)
        {
            this.game = game;
            this.levelIndex = levelIndex;
            this.stream = TitleContainer.OpenStream(@"Content\Level\0.txt");
            this.LoadAssets();
        }

        // Update
        public void Update(GameTime gameTime)
        {
            this.explorer.Update(gameTime);

            foreach (Scorpion scorpion in this.scorpions)
            {
                scorpion.Update(gameTime);
            }

            foreach (Beetle beetle in this.beetles)
            {
                beetle.Update(gameTime);
            }
        }

        // Draw
        public void Draw(GameTime gameTime)
        {
            // Het blocks array wordt getekend
            for (int row = 0; row < this.blocks.GetLength(1); row++)
            {
                for (int column = 0; column < this.blocks.GetLength(0); column++)
                {
                    this.blocks[column, row].Draw(gameTime);
                }
            }
            this.panel.Draw(gameTime);

            // Teken de scorpions
            foreach (Scorpion scorpion in this.scorpions)
            {
                scorpion.Draw(gameTime);
            }

            foreach (Beetle beetle in this.beetles)
            {
                beetle.Draw(gameTime);
            }

            foreach (Image image in this.treasures)
            {
                image.Draw(gameTime);
            }


            // De explorer wordt getekend
            this.explorer.Draw(gameTime);
        }

        private void LoadAssets()
        {
            // Maak een list<Scorpion> waarin we scorpion-objecten in kunnen opslaan
            this.scorpions = new List<Scorpion>();
            this.beetles = new List<Beetle>();
            this.treasures = new List<Image>();
            // Deze list van strings slaat elke regel van 0.txt op
            this.lines = new List<string>();

            // Het readerobject kan lezen wat er in het tekstbestand staat.
            StreamReader reader = new StreamReader(this.stream);

            string line = reader.ReadLine();

            int lineWidth = line.Length;

            while (line != null)
            {
                this.lines.Add(line);
                line = reader.ReadLine();
            }
            int amountOfLines = this.lines.Count;

            reader.Close();
            this.stream.Close();

            this.blocks = new Block[lineWidth, amountOfLines];

            // We gaan het blocks-array doorlopen met een dubbele for-lus
            for (int row = 0; row < amountOfLines; row++)
            {
                for (int column = 0; column < lineWidth; column++)
                {
                    // We lezen iedere letter uit de lines-list uit in een char variabele
                    char blockElement = this.lines[row][column];
                    this.blocks[column, row] = this.LoadBlock(blockElement, column * 32, row * 32);
                }   
            }
            ScorpionManager.Level = this;
            BeetleManager.Level = this;
            ExplorerManager.Level = this;
            ExplorerManager.Explorer = this.explorer;
        }

        public Block LoadBlock(char blockElement, int x, int y)
        {
            switch (blockElement)
            {

                case 'E':
                this.explorer = new Explorer(this.game, new Vector2(x + 16f, y + 16f));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                case 'B':
                this.beetles.Add(new Beetle(this.game, new Vector2(x + 16f, y + 16f)));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                case 'S':
                this.scorpions.Add(new Scorpion(this.game, new Vector2(x + 16f, y + 16f)));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                // Blocks
                case 'x':
                return new Block(this.game, @"Block\Wall1", new Vector2(x, y), false);

                case 'y':
                return new Block(this.game, @"Block\Wall2", new Vector2(x, y), false);

                case 'b':
                return new Block(this.game, @"Block\Block", new Vector2(x, y), false);

                // Items
                case 'p':
                this.treasures.Add(new Image(this.game, @"Items\Potion", new Vector2(x, y)));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                case 's':
                this.treasures.Add(new Image(this.game, @"Items\Scarab", new Vector2(x, y)));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                case 'a':
                this.treasures.Add(new Image(this.game, @"Items\Anc", new Vector2(x, y)));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                case '@':
                this.panel = new Panel(this.game, new Vector2(x,y));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                case 'c':
                this.treasures.Add(new Image(this.game, @"Items\Cat", new Vector2(x, y)));
                return new Block(this.game, @"Block\trans", new Vector2(x, y), true);

                // Background
                case '.':
                return new Block(this.game, @"Block\trans", new Vector2(x,y), true);
         
                default:
                return new Block(this.game, @"Block\trans", new Vector2(x,y), true);
            }
        }
    }
}
