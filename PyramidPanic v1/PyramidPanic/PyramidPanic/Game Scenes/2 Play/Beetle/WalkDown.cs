﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class WalkDown : AnimatedSprite, IEntityState
    {
        //Fields
        // Maakt de beetle aan van het type Beetle
        private Beetle beetle;

        // Maakt de velocity aan van het type Vector2
        private Vector2 velocity;

        //Constructor
        public WalkDown(Beetle beetle) : base(beetle)
        {
            this.beetle = beetle;

            // Geeft het effect de waarde FlipVertically
            this.effect = SpriteEffects.FlipVertically;

            // Geeft de destinationRectangle de waardes van de positie en grootte van de beetle
            this.destinationRectangle = new Rectangle((int)this.beetle.Position.X,
                                                      (int)this.beetle.Position.Y,
                                                       32,
                                                       32);

            // Geeft de velocity de snelheid in Y waarde
            this.velocity = new Vector2(0f, this.beetle.Speed);
        }

        // Initialize methode
        public void Initialize()
        {
            // Geeft de destination de X waarde van de beetle
            this.destinationRectangle.X = (int)this.beetle.Position.X;

            // Geeft de destination de Y waarde van de beetle
            this.destinationRectangle.Y = (int)this.beetle.Position.Y;
        }

        // Update methode
        public new void Update(GameTime gameTime)
        {
            // Laat de beetle naar beneden bewegen
            this.BeetleMoveDown();
            base.Update(gameTime);
        }

        // Draw methode
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        // Helper method voor this.BeetleMoveDown()
        private void BeetleMoveDown()
        {
            // Voert code uit als de Y positie van de beetle groter is dan 1064
            if (this.beetle.Position.Y > this.beetle.LowerBorder)
            {
                // Verandert de state naar WalkUp
                this.beetle.State = this.beetle.WalkUp;

                // Roept de Initialize methode van WalkUp.cs aan
                this.beetle.WalkUp.Initialize();
            }

            // Geeft de beetle een bewegingsnelheid
            this.beetle.Position += this.velocity;

            // Roept de Initialize methode aan van deze class
            this.Initialize();
        }
    }
}
