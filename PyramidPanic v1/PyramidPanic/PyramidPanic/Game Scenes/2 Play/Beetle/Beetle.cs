﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class Beetle : IAnimatedSprite
    {
        //Fields
        // Maakt de game aan van het type PyramidPanic
        private PyramidPanic game;

        // Maakt de state aan van het type IEntityState
        private IEntityState state;

        // Maakt de texture aan van het type Texture2D
        private Texture2D texture;

        // Maakt de position aan van het type Vector2
        private Vector2 position;

        // Maakt de speed aan met een waarde van 3 van het type int
        private int speed = 3;

        //Toestanden
        // Maakt de walkUp aan
        private WalkUp walkUp;

        // Maakt de walkDown aan
        private WalkDown walkDown;

        private int topBorder, lowerBorder;

        //Properties
        #region Properties

        // Getter voor WalkUp
        public WalkUp WalkUp
        {
            get { return this.walkUp; }
        }

        // Getter voor WalkDown
        public WalkDown WalkDown
        {
            get { return this.walkDown; }
        }

        // Setter voor State
        public IEntityState State
        {
            set { this.state = value; }
        }

        // Getter voor Game
        public PyramidPanic Game
        {
            get { return this.game; }
        }

        // Getter voor Speed
        public int Speed
        {
            get { return this.speed; }
        }

        // Getter voor Texture
        public Texture2D Texture
        {
            get { return this.texture; }
        }

        // Getter en Setter voor Position
        public Vector2 Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public int LowerBorder
        {
            get { return this.lowerBorder; }
            set { this.lowerBorder = value; }
        }

        public int TopBorder
        {
            get { return this.topBorder; }
            set { this.topBorder = value; }
        }

        #endregion

        //Constructor
        public Beetle(PyramidPanic game, Vector2 position)
        {
            this.game = game;
            this.position = position;

            // Laadt de sprite texture voor de Beetle
            this.texture = game.Content.Load<Texture2D>(@"AI\Beetle\Beetle");

            // Zorgt ervoor dat de Beetle de WalkUp state kan gebruiken
            this.walkUp = new WalkUp(this);

            // Zorgt ervoor dat de Beetle de WalkDown state kan gebruiken
            this.walkDown = new WalkDown(this);

            // Zorgt ervoor dat de Beetle in de WalkUp state begint
            this.state = new WalkUp(this);
        }

        //Update method
        public void Update(GameTime gameTime)
        {
            this.state.Update(gameTime);
        }

        //Draw method
        public void Draw(GameTime gameTime)
        {
            this.state.Draw(gameTime);
        }
    }
}
