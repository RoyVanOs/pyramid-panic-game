﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    // Dit is een toestands-Class
    public class WalkUp : AnimatedSprite, IEntityState
    {
        //Fields
        // Maakt de beetle aan van het type Beetle
        private Beetle beetle;

        // Maakt de velocity aan van het type Vector2
        private Vector2 velocity;

        //Constructor
        public WalkUp(Beetle beetle) : base(beetle)
        {
            this.beetle = beetle;

            // Geeft de destinationRectangle de waardes van de positie en grootte van de beetle
            this.destinationRectangle = new Rectangle((int)this.beetle.Position.X, 
                                                      (int)this.beetle.Position.Y,
                                                       32,
                                                       32);

            // Geeft de velocity de snelheid in Y waarde
            this.velocity = new Vector2(0f, this.beetle.Speed);
        }
        
        // Initialize methode
        public void Initialize()
        {
            // Geeft de destination de X waarde van de beetle
            this.destinationRectangle.X = (int)this.beetle.Position.X;

            // Geeft de destination de Y waarde van de beetle
            this.destinationRectangle.Y = (int)this.beetle.Position.Y;
        }

        // Update methode
        public new void Update(GameTime gameTime)
        {
            // Laat de beetle naar boven bewegen
            this.BeetleMoveUp();
            base.Update(gameTime);
        }

        // Draw methode
        public new void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        // Helper method voor this.BeetleMoveUp()
        private void BeetleMoveUp()
        {
            // Voert code uit als de Y positie van de beetle kleiner is dan 16
            if (this.beetle.Position.Y < this.beetle.TopBorder)
            {
                // Verandert de state naar WalkDown
                this.beetle.State = this.beetle.WalkDown;

                // Roept de Initialize methode van WalkDown.cs aan
                this.beetle.WalkDown.Initialize();
            }
            // Geeft de beetle een bewegingsnelheid
            this.beetle.Position -= this.velocity;

            // Roept de Initialize methode aan van deze class
            this.Initialize();
            
        }
    }
}
