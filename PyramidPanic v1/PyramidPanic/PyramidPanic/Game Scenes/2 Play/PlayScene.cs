﻿// Usings
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class PlayScene : IState
    {
        // Fields van de Start-class
        // Maakt een nieuwe instantie game aan van het type PyramidPanic
        private PyramidPanic game;

        // Maakt een nieuwe instantie background aan van het type Image
        private Image background;

        private Level level;

        // Constructor van de Start-class krijgt een object game van het type pyramid panic
        public PlayScene(PyramidPanic game)
        {
            this.game = game;

            // Roept de Initialize methode aan van deze class
            this.Initialize();
        }

        // Initialize methode, deze methode initialiseert (geeft startwaarden aan variabelen), Void wil zeggen dat er niks teruggegeven wordt.
        public void Initialize()
        {
            // Roept de LoadContent methode aan van deze class
            this.LoadContent();
        }

        // LoadContent methode, deze methode maakt nieuwe objecten aan van de verschillende klassen.
        public void LoadContent()
        {
            // Geeft de background de texture en positie
            this.background = new Image(this.game, @"PlayScene/4ksandcracked", new Vector2(-500f, 0f));

            // Maakt een level aan
            this.level = new Level(this.game, 0);
        }

        // Update methode, wordt normaal 60 maal per seconden aangeroepen.
        public void Update(GameTime gameTime)
        {
            this.level.Update(gameTime);


            // Als er op de numpad 6 toets wordt gedrukt, gaat de game naar de gameoverscene
            if (Input.EdgeDetectKeyDown(Keys.NumPad6))
            {
                // Verandert de state naar GameOverScene
                this.game.IState = this.game.GameOverScene;
            }

            // Als er op de numpad 4 toets wordt gedrukt, gaat de game naar de startscene
            if (Input.EdgeDetectKeyDown(Keys.NumPad4))
            {
                // Verandert de state naar StartScene
                this.game.IState = this.game.StartScene;
            }

            // Als er op de numpad 0 toets wordt gedrukt, gaat de game naar de startscene
            if (Input.EdgeDetectKeyDown(Keys.NumPad0))
            {
                // Roept de LoadContent methode aan van de StartScene
                this.game.StartScene.LoadContent();

                // Verandert de state naar StartScene
                this.game.IState = this.game.StartScene;   
            }
        }

        // Draw methode, deze methode wordt normaal 60 maal per seconden aangeroepen en tekent de textures op het canvas.
        public void Draw(GameTime gameTime)
        {
            // Tekent de background]
            this.background.Draw(gameTime);
            this.level.Draw(gameTime);
        }
    }
}
