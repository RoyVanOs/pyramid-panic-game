﻿// Usings
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public abstract class AnimatedSprite
    {
        //Fields
        // Maakt een instantie iAnimatedSprite aan van het type IAnimatedSprite
        private IAnimatedSprite iAnimatedSprite;

        // Maakt een instantie destinationRectangle en sourceRectangle aan van het type Rectangle
        protected Rectangle destinationRectangle, sourceRectangle;

        // Maakt een instantie pivot aan van het type Vector2
        private Vector2 pivot;

        // Maakt een instantie timer aan met een waarde van 0f van het type float
        private float timer = 0f;

        // Maakt een instantie effect aan van het type SpriteEffects, deze is wijzigbaar vanuit een andere class
        protected SpriteEffects effect;

        // Maakt een instantie rotation en timerSpeed aan van het type Float, wijzigbaar vanuit een andere class
        protected float rotation = 0f, timerSpeed = 1.5f / 60f;

        //Constructor
        public AnimatedSprite(IAnimatedSprite iAnimatedSprite)
        {
            this.iAnimatedSprite = iAnimatedSprite;

            // Geeft de waardes aan de sourceRectangle
            this.sourceRectangle = new Rectangle(0,0, 32, 32);

            // Geeft het effect een standardwaarde van None
            this.effect = SpriteEffects.None;

            // Geeft de waardes aan pivot
            this.pivot = new Vector2(16f, 16f);
        }

        //Update method
        public void Update(GameTime gameTime)
        {
            // Voert code uit als de timer groter wordt dan 10/60
            if (this.timer > 10 / 60f)
            {
                // Voert code uit als de sourceRectangle kleiner is dan 96
                if (this.sourceRectangle.X < 96)
                {
                    // Voegt 32 toe aan de sourceRectangle
                    this.sourceRectangle.X += 32;
                }
                // Als de sourceRectangle.X groter wordt dan 96, wordt dit uitgevoerd
                else
                {
                    // Zet de sourceRectangle op 0;
                    this.sourceRectangle.X = 0;
                }
                // Zet de timer op 0
                this.timer = 0f;
            }

            // Geeft de snelheid van de timer
            this.timer += timerSpeed;
        }

        //Draw method
        public void Draw(GameTime gameTime)
        {
             // Tekent de gemaakte AnimatedSprite      //Texture
             this.iAnimatedSprite.Game.SpriteBatch.Draw(this.iAnimatedSprite.Texture,

                                                        // Draw plek
                                                        this.destinationRectangle,

                                                        // Grootte sprite
                                                        this.sourceRectangle,

                                                        // Belichting sprite
                                                        Color.White,

                                                        // Rotation sprite
                                                        this.rotation,

                                                        // Draaipunt sprite
                                                        this.pivot,

                                                        // Effect sprite
                                                        this.effect,

                                                        // Diepte
                                                        0f);
        }
    }
}
