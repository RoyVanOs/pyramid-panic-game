﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class Start
    {
        // Fields van de Start-class

        // Constructor van de Start-class
        public Start()
        {

        }

        // Initialize methode, deze methode initialiseert (geeft startwaarden aan variabelen), Void wil zeggen dat er niks teruggegeven wordt.
        public void Initialize()
        {

        }

        // LoadContent methode, deze methode maakt nieuwe objecten aan van de verschillende klassen.
        public void LoadContent()
        {

        }

        // Update methode, wordt normaal 60 maal per seconden aangeroepen.
        public void Update()
        {

        }

        // Draw methode, deze methode wordt normaal 60 maal per seconden aangeroepen en tekent de textures op het canvas.
        public void Draw()
        {
   
        }
    }
}
