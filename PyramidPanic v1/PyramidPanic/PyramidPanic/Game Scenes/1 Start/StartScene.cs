﻿// Usings
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class StartScene : IState // De class StartScene implementeert de interface IState.
    {  
        // Fields van de Start-class
        private PyramidPanic game;

        // Maak een variabele (reference) aan van de Image class genaamd background en title.
        private Image background;

        // Maakt title aan van het type Image
        private Image title;

        // Maakt stars aan van het type Image
        private Image stars;

        // Maakt sky aan van het type Image
        private Image sky;

        // Maak een reference van de Menu class genaamd menu.
        private Menu menu;

        // De rotatie van een bepaalde Image
        private float rotation = 0f;

        // Dichtheid van een bepaalde Image
        private float opacity;

        // Standardwaarde voor de timer
        private float timer = 0f;

        // Constructor van de Start-class krijgt een object game van het type pyramid panic
        public StartScene(PyramidPanic game)
        {
            // Reference van de PyramidPanic class
            this.game = game;

            // Roept de initialize methode aan.
            this.Initialize();
        }

        // Initialize methode, deze methode initialiseert (geeft startwaarden aan variabelen), Void wil zeggen dat er niks teruggegeven wordt.
        public void Initialize()
        {
            // Roept de loadcontent methode aan.
            this.LoadContent();
        }

        // LoadContent methode, deze methode maakt nieuwe objecten aan van de verschillende klassen.
        public void LoadContent()
        {
            /*
             * Maakt het object background aan
             * 1ste overload = reference game
             * 2de overload = texture path
             * 3de overload = positie texture
             */
            this.background = new Image(this.game, @"StartScene/backgroundPyramid", Vector2.Zero);

            /*
             * Maakt het object title aan
             * 1ste overload = reference game
             * 2de overload = texture path
             * 3de overload = positie texture
             */
            this.title = new Image(this.game, @"StartScene/logo", new Vector2(670f, 750f));

            // Maakt het object menu aan en plaatst deze op het scherm doormiddel van gegeven overloads in de Menu class
            this.menu = new Menu(this.game);

            /*
             * Maakt het object stars en sky aan
             * 1ste overload = reference game
             * 2de overload = texture path
             * 3de overload = positie
             * 4de overload = camerapositie
             * 5de overload = kleur
             * 6de overload = rotatie
             * 7de overload = diepte
             */
            this.stars = new Image(this.game, @"StartScene/stars", new Rectangle(600, 200, 2900, 2900), new Rectangle(0, 0, 4096, 4096), Color.White, this.rotation, 1f);
            this.sky = new Image(this.game, @"StartScene/sky", new Rectangle(960, 1000, 2900, 2900), new Rectangle(0, 0, 4096, 4096), Color.White, this.rotation, 1f);
        }

        // Update methode, wordt normaal 60 maal per seconden aangeroepen.
        public void Update(GameTime gameTime)
        {
            this.menu.Update(gameTime);

            // Als er op de numpad 6 toets wordt gedrukt, gaat de game naar de playscene
            if (Input.EdgeDetectKeyDown(Keys.NumPad6))
            {
                // Zet de gamestate om naar de PlayScene
                this.game.IState = this.game.PlayScene;
            }
            // Als er op de numpad 6 toets wordt gedrukt, gaat de game naar de quitscene
            if (Input.EdgeDetectKeyDown(Keys.NumPad4))
            {
                // Zet de gamestate om naar de QuitScene
                this.game.IState = this.game.QuitScene;
            }

            // Zorgt ervoor dat de stars Image geroteerd wordt als de + toets ingedrukt wordt (DEBUGGING)
            if (Input.LevelDetectKeyDown(Keys.OemPlus))
            {
                // Geeft de rotation 60 keer minuut de waarde + 0.05 mee wanneer de + toets is ingedrukt
                this.rotation += 0.05f;
            }

            // Zorgt ervoor dat de stars Image geroteerd wordt als de - toets ingedrukt wordt (DEBUGGING)
            if (Input.LevelDetectKeyDown(Keys.OemMinus))
            {
                // Geeft de rotation 60 keer minuut de waarde - 0.05 mee wanneer de - toets is ingedrukt
                this.rotation -= 0.05f;
            }

            // Deze void methode voert een stuk code uit die ervoor zorgt dat de achtergrond van dag naar nacht verandert, of dat de sterren heel langzaam roteren
            //this.DayNightCycle();
            this.StarsRotate();
            
        }

        // Draw methode, deze methode wordt normaal 60 maal per seconden aangeroepen en tekent de textures op het canvas.
        public void Draw(GameTime gameTime)
        {
            // Tekend de sky Image.
            this.sky.DrawRotate(gameTime, 1f, this.rotation);

            // Tekend de stars Image.
            this.stars.DrawRotate(gameTime,this.opacity, this.rotation);

            // Tekend de background Image.
            this.background.Draw(gameTime);

            // Tekend de title Image.
            this.title.Draw(gameTime);
            
            // Tekend het menu.
            this.menu.Draw(gameTime);
        }

        // Methode voor de day en night cycle
        public void DayNightCycle()
        {
            // Voert een stuk code uit als de timer groter is dan 10/60
            if (this.timer > 10 / 60f)
            {
                // Voert een stuk code uit als de rotatie tussen 0 en 6.275 zit
                if ((this.rotation > 0f) && (this.rotation < 6.275f))
                {
                    // Voert een stuk code uit als de rotatie tussen 0.6 en 3.2 zit
                    if ((this.rotation > 0.6f) && (this.rotation < 3.2f))
                    {
                        // Voegt 60 keer per seconde de waarde 0.02 toe aan de opacity
                        this.opacity += 0.02f;
                    }

                    else
                    {
                        // Haalt 60 keer per seconde de waarde 0.02 van de opacity af
                        this.opacity -= 0.02f;
                    }
                }

                else
                {
                    // Zet de waarde van rotation op 0
                    this.rotation = 0f;

                    // Zet de waarde van opacity op 0
                    this.opacity = 0f;
                }

                // Voegt 60 keer per minuut 0.005 toe aan de rotatie
                this.rotation += 0.005f;

                // Zet de timer op 0
                this.timer = 0f;
            }
            this.timer += 50 / 60f;
        }

        // Methode voor de sterren te laten roteren
        public void StarsRotate()
        {
            // Geeft 60 keer per seconden de waarde 0.00045 mee aan rotation
            this.rotation += 0.00045f;

            // Maakt de opacity helemaal dicht
            this.opacity = 1f;
        }
    }
}
