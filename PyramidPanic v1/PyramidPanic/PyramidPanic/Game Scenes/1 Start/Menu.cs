﻿// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class Menu
    {
        // Fields //

        // Maak een nieuwe variabele van het type Buttons aan
        private enum Buttons { Start, Load, Help, Scores, Quit }
        //                      0       1     2     3       4

        // Maakt de textures van de knoppen aan.
        private Image start, load, help, scores, quit;

        // Maak een variabele dat een bepaalde button activeerd
        private Buttons buttonActive;

        // Maak een variabele aan ActiveColor
        private Color activeColor = Color.Gold;

        private PyramidPanic game;

        // Maak een variabele van het type List<Image>
        private List<Image> buttonList;

        // De soundeffects worden hier geinitialiseerd
        private SoundEffect click, confirm;

        // Maak een song aan van het type Song
        private Song flashinglights, sail;

        // Configuratie voor volume, pitch, en de pan van de soundeffects
        private float volume = 0.3f;
        private float pitch = 0.5f;
        private float pan = 0.3f;

        
        // Constructor
        public Menu(PyramidPanic game)
        {
            this.game = game;

            this.Initialize();
        }

        public void Initialize()
        {
            this.LoadContent();
        }

        public void LoadContent()
        {
            // Maak een instantie aan van de List<Image> type en stop deeze in de variabele this.buttonList
            this.buttonList = new List<Image>();
            // Maakt de startknop aan
            this.buttonList.Add(this.start = new Image(this.game, @"StartScene/Button_start", new Vector2(625f, 950f)));
            
            // Maakt de loadknop aan
            this.buttonList.Add(this.load = new Image(this.game, @"StartScene/Button_load", new Vector2(775f, 950f)));

            // Maakt de helpknop aan
            this.buttonList.Add(this.help = new Image(this.game, @"StartScene/Button_help", new Vector2(925f, 950f)));

            // Maakt de scoresknop aan
            this.buttonList.Add(this.scores = new Image(this.game, @"StartScene/Button_scores", new Vector2(1075f, 950f)));

            // Maakt de quitknop aan
            this.buttonList.Add(this.quit = new Image(this.game, @"StartScene/Button_quit", new Vector2(1225f, 950f)));

            // Het laden van alle soundeffects die gebruikt worden in de game
            this.click = this.game.Content.Load<SoundEffect>(@"soundfx/click01");
            this.confirm = this.game.Content.Load<SoundEffect>(@"soundfx/click02");

            // Maak het object song aan
            this.flashinglights = this.game.Content.Load<Song>(@"songs\Valleyofdeath");
            this.sail = this.game.Content.Load<Song>(@"songs\FrozenHotSauce");

            // Speelt de song af
            MediaPlayer.Play(flashinglights);

            // Herhaald de song
            MediaPlayer.IsRepeating = true;

            // Geeft de volume
            MediaPlayer.Volume = 1f; 
        }

        // Update methode
        public void Update(GameTime gameTime)
        {
            /// Deze IF instructie checked of er op de rechterpijltoets wordt gedrukt.
            /// De actie die daarop volgt is het ophogen van de variabele buttonActive
            if (Input.EdgeDetectKeyDown(Keys.Right) && buttonActive < Buttons.Quit)
            {
                // Dit zorgt ervoor dat de volgende button geselecteerd wordt
                this.ChangeButtonColorToNormal();
                this.buttonActive++;

                // Dit zorgt ervoor dat er een geluidje afspeeld als de volgende knop geselecteerd wordt
                click.Play(volume, pitch, pan);
            }

            if (Input.EdgeDetectKeyDown(Keys.Left) && buttonActive > Buttons.Start)
            {
                // Dit zorgt ervoor dat de vorige button geselecteerd wordt
                this.ChangeButtonColorToNormal();
                this.buttonActive--;

                // Dit zorgt ervoor dat er een geluidje afspeeld als de knop geselecteerd wordt
                click.Play(volume, pitch, pan);
            }

            // Maak een switch case voor de instructie voor de variabele buttonActive

            // Voert een stuk code uit als de muis met de knop rectangle kruist
            if (this.start.Rectangle.Intersects(Input.MouseRect()))
            {
                // Voert een stuk code uit als er op de linkermuisknop gedrukt wordt
                if (Input.EdgeDetectMousePressLeft())
                {
                    this.game.IState = this.game.PlayScene;

                    // Stopt alle muziek
                    MediaPlayer.Stop();

                    // Speelt "sail" af
                    MediaPlayer.Play(sail);
                    confirm.Play();
                }

                // Zet de actieve knop om naar de startknop
                this.buttonActive = Buttons.Start;

                // Verandert de kleur terug naar normaal, verwijzing naar helper method
                this.ChangeButtonColorToNormal();
                this.start.Color = this.activeColor;
            }

            // Voert een stuk code uit als de muis met de knop rectangle kruist
            else if (this.load.Rectangle.Intersects(Input.MouseRect()))
            {
                // Voert een stuk code uit als er op de linkermuisknop gedrukt wordt
                if (Input.EdgeDetectMousePressLeft())
                {
                    this.game.IState = this.game.LoadScene;
                    confirm.Play();
                }

                // Zet de actieve knop om naar de loadknop
                this.buttonActive = Buttons.Load;

                // Verandert de kleur terug naar normaal, verwijzing naar helper method
                this.ChangeButtonColorToNormal();
                this.load.Color = this.activeColor;
            }

            // Voert een stuk code uit als de muis met de knop rectangle kruist
            else if (this.help.Rectangle.Intersects(Input.MouseRect()))
            {
                // Voert een stuk code uit als er op de linkermuisknop gedrukt wordt
                if (Input.EdgeDetectMousePressLeft())
                {
                    this.game.IState = this.game.HelpScene;
                    confirm.Play();
                }

                // Zet de actieve knop om naar de helpknop
                this.buttonActive = Buttons.Help;

                // Verandert de kleur terug naar normaal, verwijzing naar helper method
                this.ChangeButtonColorToNormal();
                this.help.Color = this.activeColor;
            }

            // Voert een stuk code uit als de muis met de knop rectangle kruist
            else if (this.scores.Rectangle.Intersects(Input.MouseRect()))
            {
                // Voert een stuk code uit als er op de linkermuisknop gedrukt wordt
                if (Input.EdgeDetectMousePressLeft())
                {
                    this.game.IState = this.game.ScoreScene;
                    confirm.Play();
                }

                // Zet de actieve knop om naar de scoresknop
                this.buttonActive = Buttons.Scores;

                // Verandert de kleur terug naar normaal, verwijzing naar helper method
                this.ChangeButtonColorToNormal();
                this.scores.Color = this.activeColor;
            }

            // Voert een stuk code uit als de muis met de knop rectangle kruist
            else if (this.quit.Rectangle.Intersects(Input.MouseRect()))
            {
                // Voert een stuk code uit als er op de linkermuisknop gedrukt wordt
                if (Input.EdgeDetectMousePressLeft())
                {
                    this.game.IState = this.game.QuitScene;
                    confirm.Play();
                }

                // Zet de actieve knop om naar de quitknop
                this.buttonActive = Buttons.Quit;

                // Verandert de kleur terug naar normaal, verwijzing naar helper method
                this.ChangeButtonColorToNormal();
                this.quit.Color = this.activeColor;
            }

            else
            {
                this.ChangeButtonColorToNormal();

                switch (this.buttonActive)
                {
                    // TERNERY OPERATOR MAKEN
                    //Ternary operator: variabele = (vergelijking) ? waarde als waar, : waarde als niet waar
                    //this.game.IState = (Input.EdgeDetectKeyDown(Keys.Enter)) ? (IState)this.game.PlayScene : this.game.StartScene;


                        /////////////////////////START/////////////////////////////

                        // Begin case van de Buttons.Start
                        case Buttons.Start:

                        // Geeft de activeColor als kleur als de knop geselecteerd is
                        this.start.Color = this.activeColor;

                        // Verwijzing naar de PlayGame Helper method
                        this.PlayGame();

                        // Einde case
                        break;

                        /////////////////////////LOAD//////////////////////////////

                        // Begin case van de Buttons.Load
                        case Buttons.Load:

                        // Geeft de activeColor als kleur als de knop geselecteerd is
                        this.load.Color = this.activeColor;

                        // Verwijzing naar de LoadGame Helper method
                        this.LoadGame();

                        // Einde case
                        break;

                        ////////////////////////HELP///////////////////////////////

                        // Begin case van de Buttons.Help
                        case Buttons.Help:

                        // Geeft de activeColor als kleur als de knop geselecteerd is
                        this.help.Color = this.activeColor;

                        // Verwijzing naar de HelpGame Helper method
                        this.HelpGame();

                        // Einde case
                        break;

                        ////////////////////////SCORES/////////////////////////////

                        // Begin case van de Buttons.Scores
                        case Buttons.Scores:

                        // Geeft de activeColor als kleur als de knop geselecteerd is
                        this.scores.Color = this.activeColor;

                        // Verwijzing naar de ScoreGame Helper method
                        this.ScoreGame();

                        // Einde case
                        break;

                        /////////////////////////QUIT//////////////////////////////

                        // Begin case van de Buttons.Quit
                        case Buttons.Quit:

                        // Geeft de activeColor als kleur als de knop geselecteerd is
                        this.quit.Color = this.activeColor;

                        // Verwijzing naar de ExitGame Helper method
                        this.ExitGame();

                        // Einde case
                        break;
                }
            }
        }

        // Draw
        public void Draw(GameTime gameTime)
        {
            // Voert een stuk code uit voor ieder object in this.buttonList
            foreach (Image image in this.buttonList)
            {
                // Tekent iedere image in de lijst buttonList
                image.Draw(gameTime);
            }
        }

        // Helper method voor het met wit licht beschijnen van de button
        private void ChangeButtonColorToNormal()
        {
            /* We doorlopen de List<Image> this.buttonList met een foreach instructie
             * en we roepen voor ieder image object de property Color op en geven deze de waarde Color.white.
            */
            foreach (Image image in this.buttonList)
            {
                // Beschijnd iedere Image in de lijst buttonList
                image.Color = Color.White;
            }
        }

        // Helper method voor de een method
        private void PlayGame()
        {
            if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
            {
                // State wordt PlayScene
                this.game.IState = this.game.PlayScene;

                // Stopt alle muziek
                MediaPlayer.Stop();

                // Speelt "sail" af
                MediaPlayer.Play(sail);

                // Speelt een soundeffect "click" af
                click.Play();
            }
        }

        // Helper method voor de een method
        private void LoadGame()
        {
            // Voert code uit als de spatie ingedrukt en losgelaten wordt
            if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
            {
                // State wordt LoadScene
                this.game.IState = this.game.LoadScene;

                // Speelt een soundeffect "click" af
                confirm.Play();
            }
        }

        // Helper method voor de een method
        private void HelpGame()
        {
            // Voert code uit als de spatie ingedrukt en losgelaten wordt
            if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
            {
                // State wordt HelpScene
                this.game.IState = this.game.HelpScene;

                // Speelt een soundeffect "click" af
                confirm.Play();
            }
        }

        // Helper method voor de een method
        private void ScoreGame()
        {
            // Voert code uit als de spatie ingedrukt en losgelaten wordt
            if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
            {
                // State wordt ScoreScene
                this.game.IState = this.game.ScoreScene;

                // Speelt een soundeffect "click" af
                confirm.Play();
            }
        }

        // Helper method voor de een method
        private void ExitGame()
        {
            // Voert code uit als de spatie ingedrukt en losgelaten wordt
            if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
            {
                // State wordt QuitScene
                this.game.IState = this.game.QuitScene;

                // Speelt een soundeffect "click" af
                confirm.Play();
            }
        } 
    }
}
