﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PyramidPanic
{
    public interface IState
    {
        void Update(GameTime gameTime);
        void Draw(GameTime gameTime);
    }
}
