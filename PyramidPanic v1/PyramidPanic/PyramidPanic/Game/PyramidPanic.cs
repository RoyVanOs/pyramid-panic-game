// Met usings kun je een XNA codebibliotheek importeren in je class.
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class PyramidPanic : Microsoft.Xna.Framework.Game
    {
        // Fields
        // Maakt een instantie graphics aan van het type GraphicsDeviceManager
        private GraphicsDeviceManager graphics;

        // Maakt een instantie spriteBatch aan van het type SpriteBatch
        private SpriteBatch spriteBatch;

        // Maakt een instantie startScene aan van het type StartScene
        private StartScene startScene;

        // Maakt een instantie playScene aan van het type PlayScene
        private PlayScene playScene;

        // Maakt een instantie gameOverScene aan van het type GameOverScene
        private GameOverScene gameOverScene;

        // Maakt een instantie helpScene aan van het type HelpScene
        private HelpScene helpScene;

        // Maakt een instantie loadScene aan van het type LoadScene
        private LoadScene loadScene;

        // Maakt een instantie scoreScene aan van het type ScoreScene
        private ScoreScene scoreScene;

        // Maakt een instantie quitScene aan van het type QuitScene
        private QuitScene quitScene;

        // Maakt een instantie iState aan van het type IState.
        private IState iState;

        #region Properties
        // Properties
        // Maak de interface variabele iState beschikbaar beschikbaar buiten de class d.m.v een property IState
        public IState IState
        {
            get { return this.iState; }
            set { this.iState = value; }
        }

        // Maakt het field this.StartScene aan.
        public StartScene StartScene
        {
            get { return this.startScene; }
        }

        // Maakt het field this.PlayScene aan.
        public PlayScene PlayScene
        {
            get { return this.playScene; }
        }

        // Maakt het field this.GameOverScene aan.
        public GameOverScene GameOverScene
        {
            get { return this.gameOverScene; }
        }

        // Maakt het field this.HelpScene aan.
        public HelpScene HelpScene
        {
            get { return this.helpScene; }
        }

        // Maakt het field this.LoadScene aan.
        public LoadScene LoadScene
        {
            get { return this.loadScene; }
        }

        // Maakt het field this.ScoreScene aan.
        public ScoreScene ScoreScene
        {
            get { return this.scoreScene; }
        }

        // Maakt het field this.QuitScene aan.
        public QuitScene QuitScene
        {
            get { return this.quitScene; }
        }

        // Maak het field this.SpriteBatch aan.
        public SpriteBatch SpriteBatch
        {
            get { return this.spriteBatch; }
        }

        #endregion

        // Constructor
        public PyramidPanic()
        {
            graphics = new GraphicsDeviceManager(this);

            // Het path voor de Content map
            Content.RootDirectory = "Content";
        }

        // Initialize methode, deze methode initialiseert (geeft startwaarden aan variabelen)
        protected override void Initialize()
        {
            // Geeft een titel weer voor het scherm.
            Window.Title = "Pyramid Panic Pre-Alpha 3.114087";

            // Geeft de resolutie van het spel.
            graphics.PreferredBackBufferWidth = 1920;   //breedte
            graphics.PreferredBackBufferHeight = 1080;  //hoogte

            // Maakt de game fullscreen
            graphics.IsFullScreen = true;               

            // Maakt de muis zichtbaar.
            IsMouseVisible = true;

            // Fix voor fps drops van 60 naar 44
            IsFixedTimeStep = false;
            
            // Past de grafische wijzigingen toe.
            this.graphics.ApplyChanges();

            base.Initialize();
        }

        // LoadContent methode, deze methode laadt 
        protected override void LoadContent()
        {
            // Tekend alle textures op het scherm.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Maakt een object aan van het type StartScene
            this.startScene = new StartScene(this);

            // Maakt een object aan van het type PlayScene
            this.playScene = new PlayScene(this);

            // Maakt een object aan van het type GameOverScene
            this.gameOverScene = new GameOverScene(this);

            // Maakt een object aan van het type HelpScene
            this.helpScene = new HelpScene(this);

            // Maakt een object aan van het type LoadScene
            this.loadScene = new LoadScene(this);

            // Maakt een object aan van het type ScoreScene
            this.scoreScene = new ScoreScene(this);

            // Maakt een object aan van het type QuitScene
            this.quitScene = new QuitScene(this);

            // Deze functie zorgt ervoor dat de StartScene class als begin scene wordt gebruikt
            this.iState = this.startScene;
        }
        
        protected override void Update(GameTime gameTime)
        {
            // Exit buttons voor gamepad en keyboard, zorgt ervoor dat het spel afgesloten wordt als de back knop of escape toets wordt ingedrukt
            if ((GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed) || (Keyboard.GetState().IsKeyDown(Keys.Escape)))
            {
                // Sluit het spel af
                this.Exit();
            }
            // De Update methode van het object dat toegewezen is aan het interfaceobject this.iState wordt aangeroepen.
            this.iState.Update(gameTime);

            // De update methode van de input class wordt aangeroepen.
            Input.Update();
            
            base.Update(gameTime);
        }

        // Draw methode
        protected override void Draw(GameTime gameTime)
        {
            // Achtergrondkleur van de game
            GraphicsDevice.Clear(Color.Black);

            // Voor een spritebatch instantie iets kan tekenen moet de Begin() methode aangeroepen worden.
            this.spriteBatch.Begin();

            // De Draw methode van het object dat toegewezen is aan het interfaceobject this.iState wordt aangeroepen.
            this.iState.Draw(gameTime);

            // Nadat de spriteBatch.Draw() is aangeroepen moet de End() methode van de startscene worden aangeroepen
            this.spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
