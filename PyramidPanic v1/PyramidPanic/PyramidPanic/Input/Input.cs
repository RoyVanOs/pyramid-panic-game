﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public static class Input
    {
        // Fields
        // Mousestates voor edgedetection
        private static MouseState ms, oms;
        // Keystates voor edgedetection
        private static KeyboardState ks, oks;

        // Dit is een rectangle die aan de cursor zit geplakt
        private static Rectangle mouseRect;

        // Constructor
        static Input()
        {
            // Haalt de state op voor de keyboard
            ks = Keyboard.GetState();

            // Haalt de state op voor de mouse
            ms = Mouse.GetState();

            // oks is gelijk aan ks
            oks = ks;

            // oms is gelijk aan ms
            oms = ms;

            // De rectangle voor de muis
            mouseRect = new Rectangle(ms.X, ms.Y, 1, 1);
        }

        // Update method
        public static void Update()
        {
            // oks is gelijk aan ks
            oks = ks;

            // oms is gelijk aan ms
            oms = ms;

            // Haalt de state op voor de keyboard
            ks = Keyboard.GetState();

            // Haalt de state op voor de mouse
            ms = Mouse.GetState();
        }

        // Dit is een edgedetector voor het signaleren of een knop nu ingedrukt en de vorige update niet ingedrukt was
        public static bool EdgeDetectKeyDown(Keys key)
        {
            // Geeft terug dat een toets ingedrukt en losgelaten is
            return (ks.IsKeyDown(key) && oks.IsKeyUp(key));
        }

        // Dit is een edgedetector voor het signaleren of een knop nu niet ingedrukt en de vorige update wel ingedrukt was
        public static bool EdgeDetectKeyUp(Keys key)
        {
            // Geeft terug dat een toets losgelaten en ingedrukt is
            return (ks.IsKeyUp(key) && oks.IsKeyDown(key));
        }

        // Geeft true als de linkermuisknop ingedrukt is, en false als dit niet zo is
        public static bool EdgeDetectMousePressLeft()
        {
            // Geeft terug dat de linkermuisknop ingedrukt en losgelaten is
            return ((ms.LeftButton == ButtonState.Pressed) && (oms.LeftButton == ButtonState.Released));
        }

        // Geeft true als de rechtermuisknop ingedrukt is, en false als dit niet zo is
        public static bool EdgeDetectMousePressRight()
        {
            // Geeft terug dat de rechtermuisknop ingedrukt en losgelaten is
            return ((ms.RightButton == ButtonState.Pressed) && (oms.RightButton == ButtonState.Released));
        }

        // Dit is een leveldetector voor het detecteren of een keyboardtoets is ingedrukt
        public static bool LevelDetectKeyDown(Keys key)
        {
            // Geeft terug dat een toets is ingedrukt
            return (ks.IsKeyDown(key));
        }

        // Dit is een leveldetector voor het detecteren of een keyboardtoets niet is ingedrukt
        public static bool LevelDetectKeyUp(Keys key)
        {
            // Geeft terug dat een toets omhoog is gekomen
            return (ks.IsKeyUp(key));
        }

        // MousePosition method
        public static Vector2 MousePosition()
        {
            // Geeft de X en Y positie van de muis terug
            return new Vector2(ms.X, ms.Y);
        }

        // MouseRect method
        public static Rectangle MouseRect()
        {
            // Geeft de mouseRect.X dezelfde waarde als ms.X
            mouseRect.X = ms.X;

            // Geeft de mouseRect.Y dezelfde waarde als ms.Y
            mouseRect.Y = ms.Y;

            // Geeft de mouseRectangle terug
            return mouseRect;
        }
    }
}
