﻿// Usings
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class RotatingImage
    {
        // Fields
        private Texture2D texture;
        private Rectangle backgroundRect, cameraRect;
        private Color color;
        private float rotation = 0f;
        private float depth;


        // Maakt een variabele aan om de game instantie in op te slaan.
        private PyramidPanic game;

        // Constructor

        public RotatingImage(PyramidPanic game, string pathNameAsset, Rectangle backgroundRect, Rectangle cameraRect, Color color, float rotation, float depth)
        {
            this.game = game;
            this.texture = game.Content.Load<Texture2D>(pathNameAsset);
            this.backgroundRect = backgroundRect;
            this.cameraRect = cameraRect;
            this.color = color;
            this.rotation = rotation;
            this.depth = depth;
        }

        public void Initialize()
        {

        }

        // Update methode
        public void Update()
        {
            if ((Input.EdgeDetectKeyDown(Keys.Q) || Input.EdgeDetectKeyUp(Keys.Q)))
            {
                this.rotation += 1;
            }
        }

        // Draw methode
        public void Draw(GameTime gameTime)
        {

        }
        
        // Helper methode
    }
}
