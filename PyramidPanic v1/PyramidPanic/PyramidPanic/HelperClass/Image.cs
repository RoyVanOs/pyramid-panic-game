﻿// Usings
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PyramidPanic
{
    public class Image
    {
        // Fields

        // Maakt een texture aan.
        private Texture2D texture;

        // Maak een variabele (referece) aan van het type color met de naam color
        private Color color = Color.White;

        // Maakt een variabele aan om de game instantie in op te slaan.
        private PyramidPanic game;

        // Maakt een instantie backgroundRect en cameraRect van het type Rectangle
        private Rectangle backgroundRect, cameraRect;

        // Maakt een instantie rotation endepth van het type float
        private float rotation, depth;

        protected Rectangle rectangle;

        

        // Deze methode zorgt ervoor dat een kleurwaarde wordt opgevraagt en veranderd kan worden
        public Color Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        // Maak een property voor het rectangle field
        public Rectangle Rectangle
        {
            get { return this.rectangle; }
        }

        // Constructor
        public Image(PyramidPanic game, string pathNameAsset, Vector2 position)
        {
            this.game = game;

            // Path texture bestand
            this.texture = game.Content.Load<Texture2D>(pathNameAsset);
            this.rectangle = new Rectangle((int)position.X,
                                           (int)position.Y,
                                           this.texture.Width,
                                           this.texture.Height);
        }

        public Image(PyramidPanic game, string pathNameAsset, Rectangle backgroundRect, Rectangle cameraRect, Color color, float rotation, float depth)
        {
            this.game = game;
            this.texture = game.Content.Load<Texture2D>(pathNameAsset);
            this.backgroundRect = backgroundRect;
            this.cameraRect = cameraRect;
            this.color = color;
            this.rotation = rotation;
            this.depth = depth;
        }

        // Update methode
        public void Update()
        {

        }

        // Draw method
        public void Draw(GameTime gameTime)
        {                              //Texture
            this.game.SpriteBatch.Draw(this.texture,
                                       //Positie
                                       this.rectangle,
                                       //Belichting
                                       this.color);
        }
        
        // DrawRotate method
        public void DrawRotate(GameTime gameTime, float opacity, float rotation)
        {
            
            this.game.SpriteBatch.Draw(this.texture,
                                       //Positie
                                       this.backgroundRect,
                                       //Camera positie
                                       this.cameraRect,
                                       //Belichting * helderheid
                                       Color.White * opacity,
                                       //Rotatie
                                       rotation,
                                       //Draaipunt
                                       new Vector2(this.texture.Width / 2, this.texture.Height / 2),
                                       //Effect
                                       SpriteEffects.None,
                                       //Diepte
                                       1f);
        }
        
        // Helper methode
    }
}
