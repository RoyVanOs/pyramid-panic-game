#Pyramid Panic

* **Graphics & Sounds** used in the game `Pyramid Panic`

	- **Sprites** for AI:
		- `Explorer.png` for the main explorer character.
		- `Beetle.png` for a dangerous enemy beetle.
		- `Scorpion.png` for a dangerous enemy scorpion.
		
	- **Objects** for lootable items.
		- nothing yet.
		
	- **Sounds** for a nice background sound or a sound when interacting with something.
		- `Flashinglights.mp3` for the background sound.
		- `Click01.mp3` and `Click02.mp3` for clicking effects when interacting with a specific object.
		
* **Code examples** of what is used in `Pyramid Panic`
	
* `AnimatedSprite.cs`
	
	```c#
	public void Update(GameTime gameTime)
	{
		if (this.timer > 10 / 60f)
		{
			if (this.sourceRectangle.X < 96)
			{
				this.sourceRectangle.X += 32;
			}
			else
			{
				this.sourceRectangle.X = 0;
			}
			this.timer = 0f;
		}
		this.timer += timerSpeed;
	}
	```
	
* `Menu.cs`
	
	```c#
	private void HelpGame()
	{
		if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
		{
			this.game.IState = this.game.HelpScene;
			confirm.Play();
		}
	}

	private void ScoreGame()
	{
		if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
		{
			this.game.IState = this.game.ScoreScene;
			confirm.Play();
		}
	}

	private void ExitGame()
	{
		if ((Input.EdgeDetectKeyDown(Keys.Space) || (Input.EdgeDetectKeyUp(Keys.Space))))
		{
			this.game.IState = this.game.QuitScene;
			confirm.Play();
		}
	} 
	```
	
* `PlayScene.cs`

	```c#
	public void Update(GameTime gameTime)
	{
		this.scorpion.Update(gameTime);
		this.beetle.Update(gameTime);
		this.explorer.Update(gameTime);

		if (Input.EdgeDetectKeyDown(Keys.NumPad6))
		{
			this.game.IState = this.game.GameOverScene;
		}

		if (Input.EdgeDetectKeyDown(Keys.NumPad4))
		{
			this.game.IState = this.game.StartScene;
		}

		if (Input.EdgeDetectKeyDown(Keys.NumPad0))
		{
			this.game.IState = this.game.StartScene;
		}
	}
	```